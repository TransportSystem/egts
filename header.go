package egts

import (
	"encoding/binary"
	"errors"
)

type Header struct {
	protocolVersion uint16 // PRV
	securityKeyID   uint16 // SKID
	flags           uint16 // PRF

	length          int
	headerLength    uint16 // HL
	fieldDataLength uint16 // FDL
	packetID        uint16 // PID

	// Packet Type  - PT
	// 0 - EGTS_PT_RESPONSE (подтверждение на пакет Транспортного Уровня);
	// 1 – EGTS_PT_APPDATA (пакет содержащий данные  ППУ);
	// 2 – EGTS_PT_SIGNED_APPDATA (пакет содержащий данные  ППУс цифровой подписью)
	packetType uint16 // PT
	checkSum   uint16 // HCS
}

func ReadPacketHeader(b []byte) (h Header, err error) {

	// Header Length
	h.headerLength = uint16(b[3]) //HL длина заголовка
	if h.headerLength == 0 {
		err = errors.New("131; GetPacketHeader: HL == 0")
		return
	} else if h.headerLength < 11 {
		err = errors.New("131; GetPacketHeader: HL < 11 bytes")
		return
	}

	// Protocol Version
	h.protocolVersion = uint16(b[0])

	// Security Key ID
	h.securityKeyID = uint16(b[1])

	// Flags
	// TODO Flags
	/*
		PRF:
			Name	Bit Value
			PRF		7-6	префикс заголовка Транспортного Уровня и для данной версии должен содержать значение 00
			RTE		5		определяет необходимость дальнейшей маршрутизации = 1, то необходима
			ENA		4-3	шифрование данных из поля SFRD, значение 0 0 , то данные в поле SFRD не шифруются
			CMP		2		сжатие данных из поля SFRD, = 1, то данные в поле SFRD считаются сжатыми
			PR		1-0	приоритет маршрутизации, 1 0 – средний
	*/
	h.flags = uint16(b[2])

	// Field Data Length
	h.fieldDataLength = binary.LittleEndian.Uint16(b[5:7])

	// Packet ID
	h.packetID = binary.LittleEndian.Uint16(b[7:9])

	// Packet Type
	h.packetType = uint16(b[9])

	// CheckSum
	h.checkSum = uint16(b[10])

	// Overall PacketLength
	h.length = int(h.headerLength + h.fieldDataLength + 2)

	crc := Crc(8, b[:10])

	if int(crc) != int(h.checkSum) {
		err = errors.New("137; GetPacketHeader: Invalid Header Checksum")
	}

	return
}

func (h *Header) GetLength() int {

	return h.length
}

func (h *Header) GetPacketType() int {

	return int(h.packetType)
}

func (h *Header) GetPacketID() int {
	return int(h.packetID)
}
