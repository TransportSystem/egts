package egts

import "encoding/binary"

type Type18 struct {
	// Digital Outputs
	DigitalOutputs uint8

	// Additional Digital Inputs Octets 1-8
	ADI []int

	// Analog Sensors 1-8
	ANS []int
}

// EGTS_SR_AD_SENSORS_DATA
func (p *subRecord) type18() {

	d := &Type18{}

	d.ADI = make([]int, 8)
	d.ANS = make([]int, 8)

	// Digital Outputs
	d.DigitalOutputs = uint8(p.bytes[1])

	// DIOE1 ... DIOE8 - Digital Inputs Octet Exists
	dioeFlag := uint16(p.bytes[0])

	n := 3

	for i := 0; i < 8; i++ {
		if BitField(dioeFlag, i).(bool) {
			d.ADI[i] = int(p.bytes[n])
			n++
		} else {
			d.ADI[i] = int(-1)
		}
	}

	// ASFE1 ... ASFE8 - (Analog Sensor Field Exists)
	asfeFlag := uint16(p.bytes[2])

	for i := 0; i < 8; i++ {
		if BitField(asfeFlag, i).(bool) {
			b := append([]byte{0}, p.bytes[n:n+3]...)
			d.ANS[i] = int(binary.LittleEndian.Uint32(b))
			n += 3
		} else {
			d.ANS[i] = int(-1)
		}
	}

	p.Data = d
}
