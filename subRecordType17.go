package egts

import "encoding/binary"

type Type17 struct {
	VerticalDiluptionOfPrecision   uint16 /* Vertical Dilution of Precision */
	HorizontalDiluptionOfPrecision uint16 /* Horizontal Dilution of Precision */
	PositionDiluptionOfPrecision   uint16 /* Position Dilution of Precision */
	Satellites                     uint8  /* Satellites */
	NavigationSystem               uint16 /* Navigation System */
}

// EGTS_SR_EXT_POS_DATA_RECORD
func (p *subRecord) type17() {

	d := &Type17{}

	// Flags
	flagBytes := uint16(p.bytes[0])

	VDOP := BitField(flagBytes, 0).(bool)
	HDOP := BitField(flagBytes, 1).(bool)
	PDOP := BitField(flagBytes, 2).(bool)
	SFE := BitField(flagBytes, 3).(bool)
	NSFE := BitField(flagBytes, 4).(bool)

	n := 1

	if VDOP {
		d.VerticalDiluptionOfPrecision = binary.LittleEndian.Uint16(p.bytes[n : n+2])
		n += 2
	}
	if HDOP {
		d.HorizontalDiluptionOfPrecision = binary.LittleEndian.Uint16(p.bytes[n : n+2])
		n += 2
	}
	if PDOP {
		d.PositionDiluptionOfPrecision = binary.LittleEndian.Uint16(p.bytes[n : n+2])
		n += 2
	}
	if SFE {
		d.Satellites = uint8(p.bytes[n])
		n += 1
	}
	if NSFE {
		d.NavigationSystem = binary.LittleEndian.Uint16(p.bytes[n : n+2])
		n += 2
	}

	/*
		NS:
		0	- система не определена;
		1 - ГЛОНАСС;
		2 - GPS;
		4 - Galileo;
		8 - Compass;
		16 - Beidou;
		32 - DORIS;
		64 - IRNSS;
		128 - QZSS.
		Остальные значения зарезервированы.
	*/
	p.Data = d
}
