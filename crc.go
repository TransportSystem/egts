package egts

import (
	"github.com/sigurn/crc16"
	"github.com/sigurn/crc8"
)

func Crc(bit int, data []byte) (crc int) {

	if bit == 8 {
		table := crc8.MakeTable(crc8.Params{0x31, 0xFF, false, false, 0x00, 0xF7, "CRC-8/EGTS"})
		crc = int(crc8.Checksum(data, table))
	} else if bit == 16 {
		table := crc16.MakeTable(crc16.Params{0x1021, 0xFFFF, false, false, 0x0000, 0x29B1, "CRC-16/EGTS"})
		crc = int(crc16.Checksum(data, table))
	}
	return
}
