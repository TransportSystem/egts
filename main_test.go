package egts

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

var SamplePacket []byte

func init() {
	var err error
	SamplePacket, err = ioutil.ReadFile("./examples/tmp_packet_received.txt")
	if err != nil {
		log.Panicln(err)
	}
}

func TestGetPacketHeader(t *testing.T) {

	p := &Packet{}

	header, err := ReadPacketHeader(SamplePacket)
	if err != nil {
		t.Error(err)
	}

	if header.protocolVersion != 1 {
		t.Error("Protocol Version != 1")
	}

	if header.securityKeyID != 0 {
		t.Error("Security Key ID != 0")
	}

	if header.headerLength != 11 {
		t.Error("Header Lenght != 11", header.headerLength)
	}

	if header.fieldDataLength != 33445 {
		t.Error("Field Data Lenght != 33445", header.fieldDataLength)
	}

	if header.packetID != 1 {
		t.Error("Packet ID != 1")
	}

	if header.packetType != 1 {
		t.Error("Packet Type != 1")
	}

	_, _ = p, header

}

func TestParsePacket(t *testing.T) {

	p := &Packet{}
	err := p.Read(SamplePacket)
	if err != nil {
		t.Error(err)
	}
	/*
		b := p.GetResponse()

		_ = b
	*/

}

func BenchmarkRead(b *testing.B) {
	p := &Packet{}
	for n := 0; n < b.N; n++ {
		err := p.Read(SamplePacket)
		if err != nil {
			b.Error(err)
		}

	}
}

func BenchmarkResponse(b *testing.B) {
	p := &Packet{}
	err := p.Read(SamplePacket)
	if err != nil {
		b.Error(err)
	}

	/*
		for n := 0; n < b.N; n++ {
			res := p.GetResponse()
			_ = res
		}
	*/
}

func printJson(v interface{}) {
	fmt.Printf("_json____________________________\n")
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	os.Stdout.Write(b)
}