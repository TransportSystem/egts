package egts

type Type21 struct {
	State string /* State */

	NavigationModuleEnable bool /* Navigation Module State */
	InternalBatteryEnable  bool /* Internal Battery Used */
	BackupBatteryEnable    bool /* Back Up Battery Used */

	MainPowerSourceVoltage float32 /* Main Power Source Voltage, in 0.1V */
	BackupBatteryVoltage   float32 /* Back Up Battery Voltage, in 0.1V */
	InternalBatteryVoltage float32 /* Internal Battery Voltage, in 0.1V */
}

// EGTS_SR_STATE_DATA
func (p *subRecord) type21() {

	d := &Type21{}

	states := []string{"Idle", "EraGlonass", "Active", "EmergencyCall", "EmergencyMonitor", "Testing", "Service", "FirmwareUpdate"}

	d.State = states[int(p.bytes[0])]

	d.MainPowerSourceVoltage = float32(p.bytes[1]) * 0.1
	d.BackupBatteryVoltage = float32(p.bytes[2]) * 0.1
	d.InternalBatteryVoltage = float32(p.bytes[3]) * 0.1

	d.NavigationModuleEnable = BitField(uint16(p.bytes[4]), 0).(bool)
	d.InternalBatteryEnable = BitField(uint16(p.bytes[4]), 1).(bool)
	d.BackupBatteryEnable = BitField(uint16(p.bytes[4]), 2).(bool)

	p.Data = d
}
