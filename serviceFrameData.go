package egts

import (
	"encoding/binary"
	"errors"
	"fmt"
)

func (p *Packet) getResponseHeader() {

	p.response.packetID = binary.LittleEndian.Uint16(p.bytes[11:12])
	p.response.processingResult = uint8(p.bytes[13])

}

func (p *Packet) getServiceFrameData(i int) (err error) {

	endByte := len(p.bytes) - 2

	//CRC 16
	sfrd := p.bytes[i:endByte]
	sfrcs := p.bytes[endByte:]

	check := binary.LittleEndian.Uint16(sfrcs)

	crc := Crc(16, sfrd)
	if int(crc) != int(check) {
		err = errors.New(fmt.Sprint("138; serviceFrameData.parseSFRD: Invalid Data Checksum ", crc, check))
		return
	}

	// Service Data Record Start
	cb := sfrd
	for len(cb) > 0 {
		endByte, err = p.getServiceDataRecord(cb)
		if err != nil {
			return
		}

		if len(cb) < endByte {
			err = errors.New(fmt.Sprintf("132; getServiceDataRecord: length bytes (%d) < end byte (%d)", len(cb), endByte))
			return
		}

		cb = cb[endByte:]
	}

	return
}