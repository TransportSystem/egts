package egts

import "encoding/binary"

type subRecord struct {
	bytes []byte

	RecordType     uint8
	RecordTypeName string
	RecordLength   int

	Data interface{}
}

func ReadSubRecord(b []byte) (sr subRecord, err error) {

	sr.RecordLength = int(binary.LittleEndian.Uint16(b[1:3]))
	sr.RecordType = uint8(b[0])

	b = b[3 : sr.RecordLength+3]

	sr.bytes = b

	switch sr.RecordType {
	// EGTS_SR_RECORD_RESPONSE - Response
	case 0:
		sr.RecordTypeName = "RECORD_RESPONSE"
		//p.recordResponse()

	// EGTS_SR_POS_DATA - Positioning data
	case 16:
		sr.RecordTypeName = "POS_DATA"
		sr.type16()

	// EGTS_SR_EXT_POS_DATA - Extended Positioniug Data
	case 17:
		sr.RecordTypeName = "EXT_POS_DATA"
		sr.type17()

	// EGTS_SR_AD_SENSORS_DATA - Analog & Digital Sensors Data
	case 18:
		sr.RecordTypeName = "AD_SENSORS_DATA"
		sr.type18()

	// EGTS_SR_COUNTERS_DATA - Counters Data
	case 19:
		sr.RecordTypeName = "COUNTERS_DATA"
		sr.type19()

	// EGTS_SR_ACCEL_DATA
	case 20:
		sr.RecordTypeName = "EGTS_SR_ACCEL_DATA"

	// 	EGTS_SR_STATE_DATA - Terminal State Data (В приложении нумбер 7, оно идет под номером 20!!!)
	case 21:
		sr.RecordTypeName = "STATE_DATA"
		sr.type21()

	// EGTS_SR_LOOPIN_DATA - Status of Loop Inputs
	case 22:
		sr.RecordTypeName = "LOOPIN_DATA"

	// EGTS_SR_ABS_DIG_SENS_DATA - Status of Digital Input
	case 23:
		sr.RecordTypeName = "ABS_DIG_SENS_DATA"

	// EGTS_SR_ABS_AN_SENS_DATA - Status of Analog Input
	case 24:
		sr.RecordTypeName = "ABS_AN_SENS_DATA"

	// EGTS_SR_ABS_CNtr_DATA -Status of Counter Input
	case 25:
		sr.RecordTypeName = "ABS_CNtr_DATA"

	// EGTS_SR_ABS_LOOPIN_DATA - Status of One Loop Input
	case 26:
		sr.RecordTypeName = "ABS_LOOPIN_DATA"

	// EGTS_SR_LIQUID_LEVEL_SENSOR - Status of Liquid Sensor
	case 27:
		sr.RecordTypeName = "LIQUID_LEVEL_SENSOR"
		sr.type27()

	// EGTS_SR_PASSENGERS_COUNTERS - Passenger Counter Data
	case 28:
		sr.RecordTypeName = "PASSENGERS_COUNTERS"
	}

	return
}