package egts

import "encoding/binary"

type Type19 struct {
	Counters []int
}

// EGTS_SR_COUNTERS_DATA
func (p *subRecord) type19() {

	d := &Type19{}

	d.Counters = make([]int, 8)

	// CFE1 ... CFE8 - (Counter Field Exists)
	cfeFlag := uint16(p.bytes[0])

	n := 1
	for i := 0; i < 8; i++ {
		if BitField(cfeFlag, i).(bool) {
			b := append([]byte{0}, p.bytes[n:n+3]...)
			d.Counters[i] = int(binary.LittleEndian.Uint32(b))
			n += 3
		} else {
			d.Counters[i] = int(-1)
		}
	}

	p.Data = d
}
