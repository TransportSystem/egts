package egts

import (
	"encoding/binary"
	"errors"
	"fmt"
	"time"
)

type serviceDataRecord struct {
	recordLength        uint16
	recordNumber        int
	ObjectID            uint32
	EventID             uint32
	Time                time.Time
	SenderServiceType   uint8
	ReceiverServiceType uint8
	subRecords          []subRecord
}

func (p *Packet) getServiceDataRecord(b []byte) (Length int, err error) {

	sdr := serviceDataRecord{}

	// Record Length
	sdr.recordLength = binary.LittleEndian.Uint16(b[:2])

	// Record Number
	sdr.recordNumber = int(binary.LittleEndian.Uint16(b[2:4]))

	// Flags
	flagBytes := uint16(b[4])

	ObjectIDField := BitField(flagBytes, 0).(bool)
	EventIDField := BitField(flagBytes, 1).(bool)
	TimeField := BitField(flagBytes, 2).(bool)
	Group := BitField(flagBytes, 5).(bool)

	// Priority
	RPP := BitField(flagBytes, 3, 4).(int)

	RecipientServiceOnDevice := BitField(flagBytes, 6).(bool)
	SourceServiceOnDevice := BitField(flagBytes, 7).(bool)

	// FIXME or not
	_, _, _, _ = RPP, Group, RecipientServiceOnDevice, SourceServiceOnDevice

	// Byte Counter
	n := 5

	// Flag ObjectIDField check
	if ObjectIDField {
		// Object ID
		sdr.ObjectID = binary.LittleEndian.Uint32(b[n : n+4])
		n += 4
	}

	// Flag EventIDField check
	if EventIDField {
		// Event ID
		sdr.EventID = binary.LittleEndian.Uint32(b[n : n+4])
		n += 4
	}

	if TimeField {
		// Time
		timeUint := binary.LittleEndian.Uint32(b[n : n+4])
		t1, _ := time.Parse(
			time.RFC3339,
			"2010-01-01T00:00:00+00:00")
		sdr.Time = t1.Add(time.Duration(int(timeUint)) * time.Second)
		n += 4
	}
	// Sender Service Type
	sdr.SenderServiceType = uint8(b[n])
	n += 1

	// Receiver Service Type
	sdr.ReceiverServiceType = uint8(b[n])
	n += 1

	Length = n + int(sdr.recordLength)

	cb := b[n:Length]
	for len(cb) > 0 {
		var (
			sr subRecord
		)
		sr, err = ReadSubRecord(cb)
		if err != nil {
			return
		}

		endByte := sr.RecordLength + 3

		sdr.subRecords = append(sdr.subRecords, sr)

		if len(cb) < endByte {
			err = errors.New(fmt.Sprintf("132; getServiceDataRecord: length bytes (%d) < end byte (%d)", len(cb), endByte))
			return
		}

		cb = cb[endByte:]

	}

	p.sdr = append(p.sdr, sdr)

	return
}