package egts

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

type Packet struct {
	bytes  []byte
	header Header
	sdr    []serviceDataRecord

	response struct {
		packetID         uint16
		processingResult uint8
	}
	errorCode int64
}

func (p *Packet) GetBytes() (b []byte) {
	return p.bytes
}

func (p *Packet) GetResponseCode() (code int) {
	return int(p.errorCode)
}

func (p *Packet) Read(b []byte) (err error) {

	p.bytes = b

	p.header, err = ReadPacketHeader(b)
	if err != nil {
		return
	}

	if len(p.bytes[11:]) != int(p.header.fieldDataLength)+2 {
		p.errorCode = 132
		err = errors.New(fmt.Sprintf("serviceFrameData.parse: ServiceFrameData (len: %d) less than FieldDataLength (len: %d) in Header Data", len(p.bytes[11:]), int(p.header.fieldDataLength)))
		return
	}

	startByte := 11
	// ParseResponseHeader
	if p.header.packetType == 0 {
		p.getResponseHeader()
		startByte = 14
	}

	err = p.getServiceFrameData(startByte)
	if err != nil {
		errIndex := strings.Index(err.Error(), ";")
		if errIndex != 0 {
			p.errorCode, _ = strconv.ParseInt(err.Error()[:errIndex], 10, 64)
		}
		return
	}
	return
}

func BitField(b uint16, i ...int) interface{} {

	bitField := []int{1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768}

	var sum int

	for _, l := range i {
		sum += bitField[l]
	}

	if len(i) == 1 {
		ui := uint(i[0])
		bb := b & uint16(sum)
		f, err := strconv.ParseBool(strconv.FormatUint(uint64(bb&(1<<ui)>>ui), 10))

		if err != nil {
			log.Fatalln("BitField: strconv 1 error: ", err)
		}

		return f

	} else {

		return int(b & uint16(sum))
	}
}

type SubRecord struct {
	Type     int
	ObjectID int
	Time     time.Time
	Data     interface{}
	Bytes    []byte
}

func (p *Packet) GetSubrecords() (num []int, subRecords []*SubRecord) {
	for _, s := range p.sdr {
		num = append(num, s.recordNumber)
		for _, r := range s.subRecords {
			rr := &SubRecord{
				Type:     int(r.RecordType),
				ObjectID: int(s.ObjectID),
				Time:     s.Time,
				Data:     r.Data,
				Bytes:    r.bytes,
			}
			subRecords = append(subRecords, rr)
		}
	}
	return
}

